<html>
    
    <head>
        <title>titre</title>
        <link href="css/nav.css" rel="stylesheet">
        <link href="css/header.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/footer.css" rel="stylesheet">
    </head>
    
    <body>

        <?php
        // calls navigation
        require("php/nav.php");
        // calls header
        require("php/header.php");
        // calls main
        require("php/main.php");
        // calls footer
        require("php/footer.php");
        ?>

    </body>

</html>